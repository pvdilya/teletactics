import time
from datetime import datetime, timezone

from telethon import TelegramClient, functions
from telethon.tl.types import ChatFull, Message
from loguru import logger
from database import SessionLocal
from sqlalchemy.orm.exc import NoResultFound

from models import Channel, Post, Comment
from typing import List


class TelegramChannelParser:
    def __init__(self, username, phone, api_id, api_hash):
        self.username = username
        self.phone = phone
        self.api_id = api_id
        self.api_hash = api_hash
        self.client = TelegramClient(username, api_id, api_hash)

    async def parse_channels(self, channel_names: List[str], d_min, m_min, y_min, d_max, m_max, y_max):
        for channel_name in channel_names:
            await self._parse_single_channel(channel_name, d_min, m_min, y_min, d_max, m_max, y_max)
        return "Parsing completed for all channels"

    async def _parse_single_channel(self, channel_name, d_min, m_min, y_min, d_max, m_max, y_max, key_search=''):
        session = SessionLocal()  # Create a new database session
        try:
            channel_instance = session.query(Channel).filter(Channel.name == channel_name).one()
            if channel_instance.parsing_status == "Completed":
                logger.info(f"Channel <{channel_name}> has already been analyzed")
                return "Channel has already been parsed"
        except NoResultFound:
            # Если канала нет в БД, создаем новый экземпляр с дефолтным статусом "not analyzed"
            logger.info(f"Channel <{channel_name}> not found in database")
            channel_instance = Channel(name=channel_name, parsing_status="not analyzed")
            session.add(channel_instance)
            session.commit()
        try:
            async with self.client as client:
                # Get or create the Channel
                logger.info(f"Start parsing channel: <{channel_name}>")  # Логгирование начала парсинга
                result = await client(functions.channels.GetFullChannelRequest(channel=channel_name))

                about = str(result.full_chat.about)
                print(about)
                channel = await client.get_entity(channel_name)
                channel_instance = session.query(Channel).filter(Channel.name == channel_name).one()
                if not channel_instance:
                    channel_instance = Channel(
                        id=channel.id,
                        description=about,
                        owner_id=str(channel.creator_id) if channel.creator else None,
                        user_count=str(channel.participants_count) if hasattr(result.full_chat,
                                                                              'participants_count') else None
                    )
                    session.add(channel_instance)
                print(channel_instance)
                channel_instance.description = about
                channel_instance.owner_id = str(channel.creator_id) if channel.creator else None
                channel_instance.user_count = str(channel.participants_count) if hasattr(result.full_chat,
                                                                                         'participants_count') else None
                async for message in self.client.iter_messages(channel_name, search=key_search):
                    if datetime(y_max, m_max, d_max, tzinfo=timezone.utc) > message.date > datetime(y_min,
                                                                                                    m_min,
                                                                                                    d_min,
                                                                                                    tzinfo=timezone.utc):
                        rep_count = ''
                        # Create a new Post
                        post = Post(id_channel=message.id, content=message.text, comments_count=rep_count,
                                    channel=channel_instance)
                        session.add(post)
                        print(message)
                        if message.fwd_from:
                            post.is_forwarded = True
                            post.forwarded_from = str(message.fwd_from.from_id)
                        #  post.forwarded_from
                        # post.forwarded_from = message.
                        # Get comments for the post
                        comments = []
                        try:
                            async for comment_message in client.iter_messages(channel_name, reply_to=message.id):
                                # print(comment_message)
                                comment = Comment(id=comment_message.id,
                                                  content=comment_message.text,
                                                  post=post)
                                session.add(comment)
                                comments.append(comment)
                        except:
                            # Handle exceptions related to comments if needed
                            pass
                        channel_instance.parsing_status = "Completed"
                        session.add(channel_instance)
                        # Commit each post and its comments to the database
                        session.commit()

            logger.info(f"Parsing channel <{channel_name}> completed!")  # Логгирование завершения парсинга
            return "Data saved successfully!"

        except Exception as e:
            session.rollback()  # Rollback the changes on error
            logger.exception(f"Error during parsing channel {channel_name}: {e}")
            raise e
        finally:
            session.close()  # Close the session
