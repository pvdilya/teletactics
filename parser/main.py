import time
from datetime import datetime, timezone
import os
from dotenv import load_dotenv
from typing import List

from fastapi import FastAPI, Query

from client import TelegramChannelParser
from database import SessionLocal, Base, engine

load_dotenv()

USERNAME = os.environ.get("TELEGRAM_USERNAME")
PHONE = os.environ.get("TELEGRAM_PHONE")
API_ID = os.environ.get("TELEGRAM_API_ID")
API_HASH = os.environ.get("TELEGRAM_API_HASH")

app = FastAPI()
d_min = 1  # start day / this date will be included
m_min = 11  # start month
y_min = 2022  # start year
d_max = 1  # final day / only the day before this date will be included, that is, this date will not be included
m_max = 1  # final month
y_max = 2024  # final year

def init_db():
    Base.metadata.create_all(bind=engine)

@app.on_event("startup")
async def startup_event():
    init_db()
    global client
    client = TelegramChannelParser(USERNAME, PHONE, API_ID, API_HASH)

@app.get("/parse/")
async def parse_channels(channel_names: List[str] = Query(...)):
    data = await client.parse_channels(channel_names, d_min, m_min, y_min, d_max, m_max, y_max)
    print("Inside the endpoint")
    print(channel_names)
    return data

@app.get("/ping")
async def say_pong():
    return {"pong"}