from sqlalchemy import Integer, Column, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship, declarative_base

from database import Base

class Channel(Base):
    __tablename__ = "channels"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    owner_id = Column(String)
    user_count = Column(String)
    posts = relationship("Post", back_populates="channel")
    parsing_status = Column(String, default="not analyzed")


class Post(Base):
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True, index=True)
    id_channel = Column(Integer)
    channel_id = Column(Integer, ForeignKey('channels.id'))
    comments_count = Column(String)
    content = Column(String)
    is_forwarded = Column(Boolean, default=False)
    forwarded_from = Column(String)
    channel = relationship("Channel", back_populates="posts")
    comments = relationship("Comment", back_populates="post")



class Comment(Base):
    __tablename__ = "comments"
    id = Column(Integer, primary_key=True, index=True)
    is_reply = Column(Boolean, default=False)
    reply_to = Column(String)
    post_id = Column(Integer, ForeignKey('posts.id'))
    post = relationship("Post", back_populates="comments")
