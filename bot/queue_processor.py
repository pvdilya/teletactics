from loguru import logger
import redis
import time

class QueueProcessor:
    def __init__(self, redis_url):
        self.redis_client = redis.Redis.from_url(redis_url)
        logger.info("Queue Processor initialized")

    def process_message_queue(self, bot):
        """Processes messages from the Redis queue."""
        while True:
            try:
                _, message_data = self.redis_client.blpop('telegram_bot_queue')
                chat_id, text = message_data.decode().split(':', 1)

                time.sleep(0.5)  # Simulate processing delay
                response = f"Service response to your message: '{text}'"

                bot.send_message(chat_id, response)
                logger.info(f"Message processed and sent to user {chat_id}")
            except Exception as e:
                error_message = f"Error processing message: {e}"
                logger.error(error_message)
                bot.send_message(chat_id, error_message)
