from dotenv import load_dotenv
import os
import telebot
import threading
from queue_processor import QueueProcessor
from loguru import logger

# Setting up Loguru for logging
logger.add("bot_logs.log", format="{time} {level} {message}", level="INFO")

load_dotenv()
TOKEN = os.getenv('TELEGRAM_BOT_TOKEN')
REDIS_URL = os.getenv('REDIS_URL')
# Number of worker threads
NUM_WORKERS = os.getenv('NUM_WORKERS', '5') 

bot = telebot.TeleBot(TOKEN)
queue_processor = QueueProcessor(REDIS_URL)

def get_user_info(message):
    user = message.from_user
    username = user.username or "No Username"
    return f"{username} (Chat ID: {message.chat.id})"

@bot.message_handler(commands=['start'])
def send_welcome(message):
    user_info = get_user_info(message)
    bot.reply_to(message, "Hello! Send me something, and I will forward it to the service.")
    logger.info(f"Sent welcome message to {user_info}")

@bot.message_handler(func=lambda message: True)
def echo_all(message):
    user_info = get_user_info(message)
    try:
        queue_processor.redis_client.rpush('telegram_bot_queue', f"{message.chat.id}:{message.text}")
        bot.reply_to(message, "Your message has been added to the queue.")
        logger.info(f"Message from {user_info} added to the queue")
    except Exception as e:
        error_message = f"Error adding message to queue: {e}"
        logger.error(f"{error_message} - {user_info}")
        bot.reply_to(message, error_message)

@bot.message_handler(content_types=['photo'])
def handle_photos(message):
    user_info = get_user_info(message)
    bot.reply_to(message, "Sorry, I can't process images.")
    logger.info(f"Received an image from {user_info}, but images are not supported.")
try:
    int_num_workers = int(NUM_WORKERS)
except ValueError:
    logger.error(f"Invalid NUM_WORKERS value: {NUM_WORKERS}. Using default value 5.")
    num_workers = 5


# Starting multiple worker threads
for _ in range(int_num_workers):
    threading.Thread(target=lambda: queue_processor.process_message_queue(bot), daemon=True).start()

try:
    bot.infinity_polling()
    logger.info("Bot started and running")
except Exception as e:
    error_message = f"Error starting the bot: {e}"
    logger.error(error_message)
